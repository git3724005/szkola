main_komends = ["utwórz", "zarządzaj", "koniec"]
tworzenie_komendy = ["uczeń", "nauczyciel", "wychowawca", "koniec"]
zarzadzaj_komeny = ["klasa", "uczeń", "nauczyciel", "wychowawca", "koniec"]

uczniowie = [{"imie" : "Adam Niezgódka", "klasa" : "1A", "przedmiot" : ["matematyka" , "przyroda"]}, {"imie" : "Ewa Nośna", "klasa" : "2B"}]
nauczyciele = [{"imie" : "Eryk Sasun", "przedmiot" : "przyroda", "klasy" : ["1A", "4A"]}]
wychowawcy = [{"imie" : "Jan Bohan", "klasa" : "2B"}]

def utworz():
    koniec_tworzenie = False
    while not koniec_tworzenie:
        for i in tworzenie_komendy:
            print(i)

        komenda = input()

        if komenda == tworzenie_komendy[0]:
            uczen_imie = input("Podaj imie nowego ucznia\n")
            uczen_klasa = input("Podaj klase nowego ucznia\n")

            uczniowie.append({"imie" : uczen_imie, "klasa" : uczen_klasa})
            print(uczniowie)

        elif komenda == tworzenie_komendy[1]:
            end = False
            nauczyciel_imie = "Podaj imie nauczyciela\n"

            nauczyciel_przedmiot = "Podaj przedmiot nauczyciela\n"
            nauczyciel_klasy = []
            while not end:
                klasa = input("Podaj klase nauczyciela\n")
                if klasa:
                    nauczyciel_klasy.append(klasa)
                else:
                    end = True
            nauczyciele.append({"imie" : nauczyciel_imie, "przedmiot" : nauczyciel_przedmiot, "klasa" : nauczyciel_klasy})

        elif komenda == tworzenie_komendy[2]:
            wychowawca_imie = input("Podaj imie wychowawcy\n")
            wychowawca_klasa = input("Podaj klase której jest wychowawcą\n")

            wychowawcy.append({"imie" : wychowawca_imie, "klasa" : wychowawca_klasa})
            print(wychowawcy)

        elif komenda == tworzenie_komendy[3]:
            koniec_tworzenie = True

def zarzadzaj():
    koniec_zarzadzaj = False
    while not koniec_zarzadzaj:
        for i in zarzadzaj_komeny:
            print(i)
        komenda = input()
        if komenda == zarzadzaj_komeny[0]:
            klasa = input("Podaj klasę\n")
            print("uczniowe:")
            for j in uczniowie:
                if j["klasa"] == klasa:
                    print(j["imie"])
            print("Wychowawca:")
            for z in wychowawcy:
                if z["klasa"] == klasa:
                    print(z["imie"])
        elif komenda == zarzadzaj_komeny[1]:
            imie = input("Podaj imie ucznia\n")
            for j in uczniowie:
                if j["imie"] == imie:
                    print("Przedmioty ucznia\n")
                    for m in j["przedmiot"]:
                        print(m)
                        print("nauczyciel prowadzący")
                        for n in nauczyciele:
                            if n["przedmiot"] == m:
                                print(n["imie"])
        elif komenda == zarzadzaj_komeny[2]:
            imie = input()
            for j in nauczyciele:
                if j["imie"] == imie:
                    print(j["przedmiot"])
                    for m in j["klasa"]:
                        print(m)
        elif komenda == zarzadzaj_komeny[3]:
            imie = input()
            for j in wychowawcy:
                if j["imie"] == imie:
                    for m in uczniowie:
                        if j["klasa"] == m["klasa"]:
                            print(m["imie"])
        elif komenda == zarzadzaj_komeny[4]:
            break

def main_menu():
    global koniec_aplikacji
    for i in main_komends:
        print(i)
    komenda = input()
    if komenda == main_komends[0]:
        utworz()
    elif komenda == main_komends[1]:
        zarzadzaj()
    elif komenda == main_komends[2]:
        koniec_aplikacji = True

koniec_aplikacji = False

while not koniec_aplikacji:
    main_menu()